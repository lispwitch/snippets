#include <inttypes.h>
#include <stdio.h>

uint16_t fletcher8(uint8_t *str)
{
    uint8_t i = 0;
    uint16_t v = 0;

    while (str[i]) {
        v = (v + str[i]) % 255;
        i++;
    }

    return v;
}

void test_fletcher8(void)
{
    uint8_t input[] = "abcde";
    printf("%d\n", fletcher8(input)); // 240
    return;
}

// -------------


uint8_t lrc(uint8_t *str)
{
    uint8_t i = 0;
    uint8_t v = 0;
    while (str[i] != 0) {
        v = (v + str[i++]) & 0xff;
    }
    return ((v ^ 0xff) + 1) & 0xff;
}

void test_lrc(void)
{
    uint8_t str[] = "hello world\0";
    printf("%d\n", lrc(str)); // 164
    return;
}

// ---------------

uint8_t sum8(uint8_t *str)
{
    uint8_t i = 0;
    uint8_t v = 0;
    while (str[i] != 0) {
        v += str[i++] & 0xFF;
    }
    return v % 256;
}

void test_sum8(void)
{
    uint8_t str[] = {
        0x24, 0xD3, 
        0xA3, 0x04,
        0x6D, 0xC0, 
        0x3A, 0xF7, 
        0x2F, 0x5C,
        0x00
    };
    printf("%d\n", sum8(str)); // 135
    return;
}
