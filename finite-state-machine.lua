-- 2021-09-{09,10} - Alexandria
-- Finite State Machine demo
-- Run with Love2d
--
-- Creates a "fsm" metatable and function call, that when
-- the table is indexed (such as a function call):
-- * checks the current state, switching if necessary
-- * returns the function that's correct for the current state.
--
-- The little demo simply contains two states: "round" and "square",
-- and changes between them when the mouse is pressed

function fsm(states)
  index_metacall = function (t, key)
    function do_state(...)
      local old_current = t.current
      -- Run the state changing function
      local new_current = t[t.current][key](t.internal, ...)

      -- If the function didn't change state, exit :)
      if not new_current or old_current == new_current then
        return
      end

      -- Set our fancy new state
      t.current = new_current

      -- Otherwise, run the on_<statename> handler
      local handler = "on_" .. new_current
      if t[t.current][handler] then
        t[t.current][handler](t.internal, ...)
      end
    end

    return do_state
  end

  setmetatable(states, {__index = index_metacall})
  states.on_create(states.internal)
  return states
end

-- And now the test code!

boxen = fsm(
  {
    -- Starting state = Square
    current = "square",

    -- Function to run when the state machine is created
    on_create = function (data)
      for i = 1, 100 do
        table.insert(data.entities, {
            x = math.random(1, 800),
            y = math.random(1, 600),
            w = 32,
            h = 32,
            colour = {0, 1, 0}
        })
      end
    end,

    -- Internal state passed to functions
    internal = {entities = {}},

    -- Square state
    square = {
      draw = function (data)
        for _,box in pairs(data.entities) do
          love.graphics.setColor(box.colour)
          -- Draw the box at the center of the coordinates it is at
          local ox = box.x - box.w/2
          local oy = box.y - box.h/2
          love.graphics.rectangle("fill", ox, oy, box.w/2, box.h/2)
        end
      end,
      mousereleased = function (data)
        return "round"
      end,
    },

    -- Round state
    round = {
      draw = function (data)
        for _,box in pairs(data.entities) do
          love.graphics.setColor(box.colour)
          -- Draw the circle at the center of the coordinates it is at
          local ox = box.x - box.w/2
          local oy = box.y - box.w/2
          love.graphics.circle("fill", ox, oy, box.w/2)
        end
      end,
      mousereleased = function (data)
        return "square"
      end
    }

  }
)

function love.mousereleased(key)
  boxen.mousereleased(key)
end

function love.draw()
  boxen.draw()
end
