;;; package -- Summary
; From: http://xahlee.info/emacs/emacs/emacs_open_file_path_fast.html
; Date: 2021-11-23T11:40Z
; Author: Alexandria Asternova
;
; Open URL/File Path under cursor (or selected) in current browser/buffer.
; If a line number is present, scroll to that point.
;
; From Xah but rewritten to remove some cruft and just make it much easier to maintain and read. Yeesh.

;;; Commentary:
;
; Place in `xah-open-file.el' in `~/.doom.d/' and load using (load-file "./xah-open-file.el")
;
; If you want binding to work in vterm, you have to use a leader key unfortunately. Maybe shift-enter would work?
; I have: (map! :n "f RET" #'xah/open-file-at-cursor)

;;; Code:
(defun my/clean-path-string (s)
  "Convert a file path string into a valid one that can be opened"
  (let* ((sa (replace-regexp-in-string ":\\'" "" s))
         (sb (replace-regexp-in-string "^file:///" "/" sa))
         (sc (replace-regexp-in-string "^~"
                                       (getenv "HOME")
                                       sb))
         (sd (replace-regexp-in-string "\\([[:digit:]]+?\\):[[:alnum:]]+\\'"
                                       "\\1"
                                       sc)))
         sd))

(defun my/grab-region (path-stops)
  "Return a region of text at cursor. The region is either selected, or surrounded within PATH-STOPS."
  (if (use-region-p)
      (buffer-substring-no-properties (region-beginning) (region-end))
    (let (origin start end)
      (setq origin (point))
      (skip-chars-backward path-stops)
      (setq start (point))
      (goto-char origin)
      (skip-chars-forward path-stops)
      (setq end (point))
      (goto-char origin)
      (buffer-substring-no-properties start end))))

(defun my/is-url? (s)
  "Is the given string S a URL?"
  (string-match-p "\\`https?://" s))

;; This does not work, probably because the matches are not returned in the current "evaluation context" hmm
;; (defun my/is-file? (s)
;;   "Matches a file path with optional trailing line numbers.
;;    Returns two captures, obtain them with (match-string)
;;    Capture 1: File path
;;    Capture 2: Line number"
;;   (string-match "^\\`\\(.+?\\):\\([0-9]+\\)\\(:[0-9]+\\)?\\'" s))

(defun my/ask-create-file (path)
  "Informs the user that the given PATH does not exist and asks to create it."
  (when (y-or-n-p (format "File does not exist: 「%s」. Create?" path))
    (find-file path)))

(defun my/open-file-at-line (path line-number)
  "Opens PATH in the current buffer and scrolls to the LINE-NUMBER provided."
  (find-file path)
  (goto-char (point-min))
  (forward-line (1- (or (string-to-number line-number) 0))))

(defun my/open-or-create-file (path line-number)
  "Optionally either open the file at PATH at LINE-NUMBER, or prompt for it's creation."
  (let* ((project-root (projectile-acquire-root))
         (project-path (string-join (list project-root path) "")))
    (cond
     ((file-exists-p path)
      (my/open-file-at-line path line-number))
     ((file-exists-p project-path)
      (my/open-file-at-line project-path line-number))
     (t (my/ask-create-file path)))))

(defun xah/open-file-at-cursor ()
  "Open the file path under cursor.
If there is text selection, uses the text selection for path.
If the path starts with “http://”, open the URL in browser.
Input path can be {relative, full path, URL}.
Path may have a trailing “:‹n›” that indicates line number, or “:‹n›:‹m›” with line and column number. If so, jump to that line number.
If path does not have a file extension, automatically try with “.el” for elisp files.
This command is similar to `find-file-at-point' but without prompting for confirmation.

URL `http://xahlee.info/emacs/emacs/emacs_open_file_path_fast.html'
Version 2020-10-17"
  (interactive)
   ;; chars that are likely to be delimiters of file path or url, e.g. whitespace, comma.
   ;; The colon is a problem. cuz it's in url, but not in file name. Don't want to use just space
   ;; as delimiter because path or url are often in brackets or quotes as in markdown or html
  (let* ((path-stops "^  \t\n\"`'‘’“”|[]{}「」<>〔〕〈〉《》【】〖〗«»‹›❮❯❬❭〘〙·。\\")
         (input (my/grab-region path-stops))
         (path (my/clean-path-string input)))
    (cond ((my/is-url? path)
           (browse-url path))
          ((string-match "\\`\\(.+?\\):\\([0-9]+\\)?.*" path)
           (my/open-or-create-file (match-string 1 path)
                                   (match-string 2 path)))
          (t (my/open-or-create-file path 0)))))

;;; xah-open-file.el ends here
